const unit = 'px'
const breakpoints = {
  xs: 480,
  sm: 768,
  md: 992,
  lg: 1200,
  xl: 1400,
}

export const minBreakpoint = (key, css) => ({
  [`@media (min-width: ${breakpoints[key]}${unit})`]: { ...css },
})

export const maxBreakpoint = (key, css) => ({
  [`@media (max-width: ${breakpoints[key] - 1}${unit})`]: { ...css },
})

export const breakpointKeys = Object.keys(breakpoints)
  .map(key => ({ value: breakpoints[key], key }))
  .sort((a, b) => a.bp - b.bp)
  .map(({ key }) => key)

export default breakpoints
