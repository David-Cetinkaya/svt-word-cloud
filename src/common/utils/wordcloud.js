const numberWithinLimit = (number, min, max) =>
  Math.min(Math.max(number, min), max)

const removeStopWords = (words, stopWords) =>
  words.map(w => w.toLowerCase()).filter(w => stopWords.indexOf(w) === -1)

const wordMapFrom = words =>
  words.reduce((wordMap, key) => {
    const count = (wordMap[key] || 0) + 1
    return { ...wordMap, [key]: count }
  }, {})

const mostCommonWordsIn = (wordMap, wordLimit) =>
  Object.keys(wordMap)
    .map(word => ({ word, count: wordMap[word] }))
    .sort((a, b) => b.count - a.count)
    .slice(0, wordLimit)

const withFrequency = words => {
  const frequecy = w => w.count / (words[0].count / words.length) / 100
  return words.map(w => ({ ...w, freq: frequecy(w) }))
}

const withProperty = (words, prop, min, max) =>
  words.map(word => {
    const desiredValue = max * word.freq
    const value = numberWithinLimit(desiredValue, min, max)
    return { ...word, [prop]: value }
  })

const shuffle = array => {
  for (let i = array.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1))
    ;[array[i], array[j]] = [array[j], array[i]]
  }
  return array
}

const createWordCloud = (words, stopWords) => {
  const wordsWithoutStopWords = removeStopWords(words, stopWords)
  const wordMap = wordMapFrom(wordsWithoutStopWords)
  const wordsMostCommon = mostCommonWordsIn(wordMap, 100)
  const wordsWithFrequency = withFrequency(wordsMostCommon)
  const wordsWithFontSize = withProperty(wordsWithFrequency, 'fontSize', 10, 50)
  const wordsWithOpacity = withProperty(wordsWithFontSize, 'opacity', 0.05, 1)
  const shuffledWords = shuffle(wordsWithOpacity)
  return shuffledWords
}

export default createWordCloud
