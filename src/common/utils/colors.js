const colors = {
  brand: '#00C802',
  london: '#000000',
  prague: '#0b0c0d',
  vienna: '#ffffff',
}

export default colors
