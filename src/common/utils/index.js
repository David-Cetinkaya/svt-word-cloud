export const randomNumber = max => {
  const num = Math.floor(Math.random() * max) + 1
  const direction = Math.floor(Math.random() * 2) + 1
  return direction === 2 ? num : num * -1
}
