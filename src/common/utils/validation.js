const testRegexFor = type => string => regexp[type].test(string)
const regexp = {
  url: /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-/]))?/,
  hashtag: /(#[a-z][a-z\d-]+)/,
}

export const validHashtag = testRegexFor('hashtag')
export const validUrl = testRegexFor('url')
