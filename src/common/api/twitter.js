import Twitter from 'twitter'

const client = new Twitter({
  consumer_key: process.env.RAZZLE_TWITTER_API_KEY,
  consumer_secret: process.env.RAZZLE_TWITTER_API_SECRET_KEY,
  access_token_key: process.env.RAZZLE_TWITTER_ACCESS_TOKEN,
  access_token_secret: process.env.RAZZLE_TWITTER_ACCESS_TOKEN_SECRET,
})

export const queryTweets = async query => {
  const tweets = await client
    .get('search/tweets', { q: query, count: 100 })
    .then(tweets => tweets)
    .catch(error => {
      throw Error(error)
    })
  return tweets
}

export default client
