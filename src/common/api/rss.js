import RssParser from 'rss-parser'

const client = new RssParser()

export const queryRssFeed = async query => {
  const feed = await client.parseURL(query)
  return feed
}

export default client
