import React from 'react'
import { styled } from 'styletron-react'
import { minBreakpoint } from '../utils/breakpoints'

const Wrap = styled('div', {
  marginLeft: 'auto',
  marginRight: 'auto',
  maxWidth: '1250px',
  paddingLeft: '15px',
  paddingRight: '15px',
  ...minBreakpoint('sm', {
    paddingLeft: '35px',
    paddingRight: '35px',
  }),
})

const SiteWrap = ({ children }) => <Wrap>{children}</Wrap>

export default SiteWrap
