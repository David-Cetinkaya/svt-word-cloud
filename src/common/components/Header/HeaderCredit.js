import React from 'react'
import { minBreakpoint } from '../../utils/breakpoints'
import { styled } from 'styletron-react'

const Credit = styled('div', {
  flex: '1 0 0',
  textAlign: 'right',
  fontSize: '14px',
  marginLeft: '15px',
  ...minBreakpoint('sm', {
    fontSize: '16px',
  }),
})

const Name = styled('span', {
  fontWeight: 'bold',
})

const HeaderCredit = () => (
  <Credit>
    Tag cloud by <Name>David Cetinkaya</Name>
  </Credit>
)

export default HeaderCredit
