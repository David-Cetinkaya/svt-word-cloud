import React from 'react'
import { styled } from 'styletron-react'
import colors from '../../utils/colors'
import Logo from './HeaderLogo'
import Credit from './HeaderCredit'
import SiteWrap from '../SiteWrap'

const Wrap = styled('header', {
  backgroundColor: colors.london,
  color: colors.vienna,
  paddingTop: '10px',
  paddingBottom: '10px',
})

const Inner = styled('header', {
  display: 'flex',
  alignItems: 'center',
})

const Header = () => (
  <Wrap>
    <SiteWrap>
      <Inner>
        <Logo />
        <Credit />
      </Inner>
    </SiteWrap>
  </Wrap>
)

export default Header
