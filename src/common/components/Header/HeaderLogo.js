import React from 'react'
import { styled } from 'styletron-react'
import colors from '../../utils/colors'
import { Link } from 'react-router-dom'
import Svg from '../Svg/Svg'

const LogoLink = styled(Link, {
  display: 'block',
  flex: '0 0 auto',
  width: '80px',
  height: '34px',
  outline: 'none',
  ':focus': {
    boxShadow: `inset 0 0 0 2px ${colors.brand}`,
  },
})

const HeaderLogo = () => (
  <LogoLink to="/">
    <Svg icon="logo" fill={colors.vienna} />
  </LogoLink>
)

export default HeaderLogo
