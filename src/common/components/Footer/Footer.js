import React from 'react'
import { styled } from 'styletron-react'
import colors from '../../utils/colors'
import SiteWrap from '../SiteWrap'
import ContactInfo from './FooterContactInfo'

const Wrap = styled('footer', {
  backgroundColor: colors.london,
  color: colors.vienna,
  paddingTop: '10px',
  paddingBottom: '10px',
  position: 'absolute',
  bottom: 0,
  width: '100%',
})

const Footer = () => (
  <Wrap>
    <SiteWrap>
      <ContactInfo />
    </SiteWrap>
  </Wrap>
)

export default Footer
