import React from 'react'
import { styled } from 'styletron-react'
import { minBreakpoint } from '../../utils/breakpoints'
import colors from '../../utils/colors'
import Svg from '../Svg/Svg'

const Wrap = styled('ul', {
  marginLeft: '-30px',
  marginBottom: '-10px',
  ...minBreakpoint('xs', {
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  }),
})

const LinkItem = styled('li', {
  paddingLeft: '30px',
  paddingBottom: '10px',
})

const Link = styled('a', {
  display: 'flex',
  color: 'currentColor',
  textDecoration: 'none',
  alignItems: 'center',
  justifyContent: 'center',
  outline: 'none',
  ':hover': {
    textDecoration: 'underline',
  },
  ':focus': {
    boxShadow: `inset 0 0 0 2px ${colors.brand}`,
  },
})

const LinkText = styled('span', {
  display: 'block',
  paddingLeft: '10px',
})

const ContactInfo = () => (
  <Wrap>
    <LinkItem>
      <Link href="mailto:david.o.cetinkaya@gmail.com">
        <Svg icon="email" fill="currentColor" width={21} height={16} />
        <LinkText>david.o.cetinkaya@gmail.com</LinkText>
      </Link>
    </LinkItem>
    <LinkItem>
      <Link href="tel:+46762163332">
        <Svg icon="phone" fill="currentColor" width={16} height={16} />
        <LinkText>0762 16 33 32</LinkText>
      </Link>
    </LinkItem>
  </Wrap>
)

export default ContactInfo
