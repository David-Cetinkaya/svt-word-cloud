import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { styled } from 'styletron-react'
import { minBreakpoint } from '../utils/breakpoints'
import Header from './Header/Header'
import WordCloud from './WordCloud/WordCloudContainer'
import Footer from './Footer/Footer'

const Wrap = styled('div', {
  minHeight: '100%',
  position: 'relative',
})

const Main = styled('main', {
  paddingBottom: '70px',
  ...minBreakpoint('xs', {
    paddingBottom: '40px',
  }),
})

const App = () => (
  <Wrap>
    <Header />
    <Main>
      <Switch>
        <Route path="/" component={WordCloud} />
      </Switch>
    </Main>
    <Footer />
  </Wrap>
)

export default App
