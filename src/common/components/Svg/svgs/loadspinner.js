import React from 'react'

export default {
  viewBox: '0 0 38 38',
  path: (
    <g
      transform="translate(1 1)"
      strokeWidth={2}
      fill="none"
      fillRule="evenodd"
    >
      <circle strokeOpacity={0.5} cx={18} cy={18} r={18} />
      <path d="M36 18c0-9.94-8.06-18-18-18" />
    </g>
  ),
}
