import React from 'react'
import logo from './svgs/logo'
import search from './svgs/search'
import loadspinner from './svgs/loadspinner'
import phone from './svgs/phone'
import email from './svgs/email'

const svgMap = {
  logo,
  search,
  loadspinner,
  email,
  phone,
}

const Svg = ({ icon, ...props }) => {
  const { viewBox, path } = svgMap[icon]
  return (
    <svg viewBox={viewBox} width="100%" height="100%" {...props}>
      {path}
    </svg>
  )
}

export default Svg
