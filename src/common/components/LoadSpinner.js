import React from 'react'
import { styled } from 'styletron-react'
import colors from '../utils/colors'
import Svg from './Svg/Svg'

const spin = {
  from: { transform: 'rotate(0deg)' },
  to: { transform: 'rotate(360deg)' },
}

const Spinner = styled(Svg, {
  animationName: spin,
  animationDuration: '1.2s',
  animationIterationCount: 'infinite',
  animationTimingFunction: 'linear',
})

const LoadSpinner = ({ color = colors.vienna, ...props }) => (
  <Spinner icon="loadspinner" stroke={color} {...props} />
)

export default LoadSpinner
