import React from 'react'
import { styled } from 'styletron-react'
import { minBreakpoint } from '../../utils/breakpoints'
import createWordCloud from '../../utils/wordcloud'
import LoadSpinner from '../LoadSpinner'
import { randomNumber } from '../../utils'
import colors from '../../utils/colors'
import stopWords from './stopWords'

const Wrap = styled('div', {
  color: colors.vienna,
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
  paddingTop: '15px',
  paddingBottom: '15px',
  textAlign: 'center',
  ...minBreakpoint('sm', {
    paddingTop: '45px',
    paddingBottom: '45px',
  }),
})

const Cloud = styled('div', {
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
  marginLeft: 'auto',
  marginRight: 'auto',
  fontSize: '8em',
  ...minBreakpoint('sm', {
    fontSize: '12em',
  }),
})

const CloudItem = styled(
  'div',
  ({ $fontSize, $opacity, $translateY, $rotate }) => ({
    fontSize: `${$fontSize}%`,
    opacity: $opacity,
    lineHeight: '60px',
    paddingLeft: '5px',
    paddingRight: '5px',
    transform: `translateY(${$translateY}px) rotate(${$rotate}deg)`,
  }),
)

const WordCloud = ({ error, loading, words }) => {
  if (error) return <Wrap>{error}</Wrap>
  if (loading) {
    return (
      <Wrap>
        <LoadSpinner width={60} height={60} />
      </Wrap>
    )
  }

  return (
    <Wrap>
      <Cloud>
        {createWordCloud(words, stopWords).map(
          ({ word, fontSize, opacity }) => (
            <CloudItem
              key={word}
              $fontSize={fontSize}
              $translateY={randomNumber(10)}
              $rotate={randomNumber(2)}
              $opacity={opacity}
            >
              {word}
            </CloudItem>
          ),
        )}
      </Cloud>
    </Wrap>
  )
}

export default WordCloud
