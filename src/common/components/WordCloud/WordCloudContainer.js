import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'
import { validUrl, validHashtag } from '../../utils/validation'
import WordCloudForm from './WordCloudForm'
import WordCloud from './WordCloud'
import SiteWrap from '../SiteWrap'

class WordCloudContainer extends Component {
  state = {
    data: [],
    error: '',
    search: '',
    loading: false,
  }

  readQuery = () => {
    const search = this.props.location.search
    return queryString.parse(search).q || ''
  }

  endPoint = query => {
    if (validUrl(query)) return 'rss'
    if (validHashtag(query)) return 'twitter'
    return ''
  }

  fetchSearchResults = query => {
    const endPoint = this.endPoint(query)

    if (!endPoint && query) {
      const message = 'Please enter a valid Twitter Hashtag or RSS Link'
      this.setState({ error: message, loading: false })
    }

    if (endPoint && query !== this.state.search) {
      const q = encodeURIComponent(query)
      const requestUrl = `http://localhost:4000/${endPoint}?q=${q}`
      this.setState({ error: '', loading: true, data: [] })
      fetch(requestUrl)
        .then(this.onFetchResponse)
        .then(this.onFetchSuccess)
        .catch(this.onFetchError)
    }
    this.setState({ search: this.readQuery() })
  }

  onFetchResponse = res => {
    const body = res.json()
    if (res.status !== 200) {
      this.onFetchError()
      throw Error(body.message)
    }
    return body
  }

  onFetchSuccess = data => {
    this.setState({ data, loading: false })
  }

  onFetchError = () => {
    const message = 'Something went wrong...'
    this.setState({ error: message, loading: false })
  }

  onRouteChanged() {
    this.setState({ data: [], loading: false })
    this.fetchSearchResults(this.readQuery())
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged()
    }
  }

  componentDidMount = () => {
    this.onRouteChanged()
  }

  render() {
    const { search, data, error, loading } = this.state

    return (
      <SiteWrap>
        <WordCloudForm
          fetchSearchResults={this.fetchSearchResults}
          history={this.props.history}
          search={search}
        />
        <WordCloud words={data} error={error} loading={loading} />
      </SiteWrap>
    )
  }
}

export default withRouter(WordCloudContainer)
