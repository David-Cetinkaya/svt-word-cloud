import React, { Component } from 'react'
import { styled } from 'styletron-react'
import colors from '../../utils/colors'
import Svg from '../Svg/Svg'

const Wrap = styled('div', {
  paddingTop: '15px',
  maxWidth: '350px',
  width: '100%',
  marginLeft: 'auto',
  marginRight: 'auto',
  display: 'flex',
})

const Form = styled('form', {
  position: 'relative',
  width: '100%',
})

const Input = styled('input', {
  borderRadius: '19px',
  backgroundColor: colors.vienna,
  border: 0,
  height: '33px',
  width: '100%',
  paddingLeft: '15px',
  paddingRight: '38px',
  outline: 'none',
  '-webkit-appearance': 'none',
  ':focus': {
    boxShadow: `inset 0 0 0 2px ${colors.brand}`,
  },
})

const Submit = styled('button', {
  position: 'absolute',
  backgroundColor: 'transparent',
  border: 0,
  top: 0,
  right: '5px',
  width: '33px',
  height: '33px',
  cursor: 'pointer',
  outline: 'none',
  ':focus': {
    boxShadow: `inset 0 0 0 2px ${colors.brand}`,
  },
})

class WordCloudForm extends Component {
  state = { value: '' }

  onChange = e => {
    const value = e.target.value
    this.setState({ value })
  }

  onSubmit = e => {
    e.preventDefault()
    const { value } = this.state
    const { search, history, fetchSearchResults } = this.props
    if (value && value !== search) {
      const query = encodeURIComponent(value)
      history.push(`/?q=${query}`)
      fetchSearchResults(query)
    }
  }

  render() {
    const { state, onSubmit, onChange } = this
    return (
      <Wrap>
        <Form onSubmit={onSubmit}>
          <Input
            type="text"
            value={state.value}
            onChange={onChange}
            placeholder="Search for Hashtag or RSS Link"
          />
          <Submit type="submit">
            <Svg icon="search" fill="currentColor" />
          </Submit>
        </Form>
      </Wrap>
    )
  }
}

export default WordCloudForm
