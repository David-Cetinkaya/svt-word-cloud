import App from '../common/components/App'
import React from 'react'
import { hydrate } from 'react-dom'
import { Provider as StyletronProvider } from 'styletron-react'
import { Client as Styletron } from 'styletron-engine-atomic'
import { BrowserRouter } from 'react-router-dom'

const root = document.getElementById('root')
const styles = document.getElementsByClassName('_styletron_hydrate_')
const styletronEngine = new Styletron({ hydrate: styles })
const markup = (
  <StyletronProvider value={styletronEngine}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </StyletronProvider>
)

hydrate(markup, root)

if (module.hot) {
  module.hot.accept()
}
