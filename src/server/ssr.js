import App from '../common/components/App'
import React from 'react'
import express from 'express'
import { renderToString } from 'react-dom/server'
import { Server as Styletron } from 'styletron-engine-atomic'
import { Provider as StyletronProvider } from 'styletron-react'
import { StaticRouter } from 'react-router-dom'
import rootCss from './root.css'

const isProduction = process.env.NODE_ENV === 'production'
const assets = require(process.env.RAZZLE_ASSETS_MANIFEST)
const app = express()

const htmlDocument = (markup, css) => {
  const globalCss = rootCss.toString().replace(/\n|\s/g, '')

  return `
    <!doctype html>
    <html lang="en">
      <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <title>SVT Tag Cloud</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style id="global-css">${globalCss}</style>
        ${css}
      </head>
      <body>
        <div id="root">${markup}</div>
        ${`<script src="${assets.client.js}"${
          isProduction ? ' crossorigin' : ''
        } defer></script>`}
    </body>
  </html>
  `
}

app
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', (req, res) => {
    const styletronEngine = new Styletron()
    const context = {}
    const markup = renderToString(
      <StyletronProvider value={styletronEngine}>
        <StaticRouter context={context} location={req.url}>
          <App />
        </StaticRouter>
      </StyletronProvider>,
    )

    if (context.url) {
      res.redirect(context.url)
    } else {
      const css = styletronEngine.getStylesheetsHtml()
      res.status(200).send(htmlDocument(markup, css))
    }
  })

export default app
