import cors from 'cors'
import express from 'express'
import bodyParser from 'body-parser'
import { queryTweets } from '../common/api/twitter'
import { queryRssFeed } from '../common/api/rss'

const app = express()
const port = 4000

app.listen(port)
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/twitter', async (req, res) => {
  const query = req.query.q || ''
  const tweets = await queryTweets(query)
  return res.status(200).json(
    tweets.statuses
      .map(status => status.text)
      .join()
      .split(' ')
      .filter(words => words.match(/^[a-zA-Z]+$/)),
  )
})

app.use('/rss', async (req, res) => {
  const query = req.query.q || ''
  const feed = await queryRssFeed(query)
  return res.status(200).json(
    feed.items
      .map(item => item.content)
      .join()
      .split(' ')
      .filter(words => words.match(/^[a-zA-Z]+$/)),
  )
})

export default app
